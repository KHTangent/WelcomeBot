import { readFileSync, existsSync, lstatSync, lstat } from "fs";
import { join as pathJoin } from "path";

export function loadConfig():config {
	var file;
	try {
		file = readFileSync(pathJoin(__dirname, "../config.json"));
	}
	catch {
		console.log("Could not load config.json");
		process.exit();
	}
	var parsed = JSON.parse(file.toString());
	if (!parsed.token) {
		console.log("Please provide a bot token.");
		process.exit();
	}
	var soundsDir = (parsed.soundsDir ? pathJoin(parsed.soundsDir) : pathJoin(__dirname, "../sounds"));
	if (!existsSync(soundsDir) || !lstatSync(soundsDir).isDirectory()) {
		console.log(`Directory ${soundsDir} not found`);
		process.exit();
	}
	return {
		token: parsed.token,
		soundsDir: soundsDir,
		prefix: parsed.prefix || "''"
	}
}


interface config {
	token:string;
	soundsDir:string;
	prefix:string;
}
