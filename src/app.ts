import Discord = require("discord.js");
import { join as pathJoin } from "path";
import { readdir, readFile } from "fs";
import { loadConfig } from "./ConfigLoader";


console.log("Starting WelcomeBot...");
console.log("Loading config.json...");
const config = loadConfig();
console.log("Config loaded. Connecting to Discord...");

const bot = new Discord.Client();

bot.on("ready", () => {
	console.log("Connected to Discord");
});

bot.on("message", async msg => {
	if (!msg.content.startsWith(config.prefix)) return;

	if (msg.content == `${config.prefix}ping`) {
		msg.channel.send("Pong!");
	}

	if (msg.content == `${config.prefix}joinMV` && msg.guild) {
		if (msg.member?.voice.channel) {
			try {
				msg.member.voice.channel.join();
			} catch (e) {
				msg.channel.send("Could not join voice channel");
			}
		}
		else {
			msg.channel.send("You must be in a voice channel to use this command");
		}
	}

	else if (msg.content == `${config.prefix}plRand` && msg.guild) {
		var relevantConnection = bot.voice?.connections.find(c => c.channel.guild.id == msg.guild!.id);
		if (relevantConnection) {
			playRandomSound(relevantConnection);
		}
	}

	else if (msg.content == `${config.prefix}lsSounds`) {
		readdir(config.soundsDir, (err, files) => {
			if (err) {
				console.error(err);
				return;
			}
			var message = `List of ${files.length} sounds: \`\`\`${files.map(e => e.split(".")[0]).join(" ")}\`\`\``;
			msg.channel.send(message);
		});
	}

	else if (msg.content.startsWith(`${config.prefix}plSnd`) && msg.guild) {
		var relevantConnection = bot.voice?.connections.find(c => c.channel.guild.id == msg.guild!.id);
		if (relevantConnection) {
			readdir(config.soundsDir, (err, files) => {
				if (err) {
					console.error(err);
					return;
				}
				var search = msg.content.substring(`${config.prefix}plSnd`.length).trim();
				var file = files.find(e => e.startsWith(search));
				if (!file) {
					msg.channel.send(`No sound found for \`${search}\``);
					return;
				}
				try {
					relevantConnection?.play(pathJoin(config.soundsDir, file));
				} catch (e) {
					console.error(e);
				}
			});
		}
	}
});


bot.on("voiceStateUpdate", (oldState, newState) => {
	// User joined (old channel is different from new channel, new channel is defined, and it's not this bot)
	if (oldState.channelID != newState.channelID 
		&& newState.channelID 
		&& !newState.member?.user.bot) {
		var relevantConnection = bot.voice?.connections.find(c => c.channel.id == newState.channelID);
		if (relevantConnection) { // Bot is connected to this channel
			playRandomSound(relevantConnection);
		}
	}
});



bot.login(config.token);



function playRandomSound(connection:Discord.VoiceConnection) {
	readdir(config.soundsDir, (err, files) => {
		if (err) {
			console.error(err);
			return;
		}
		var filePath = pathJoin(config.soundsDir, files[randInt(0, files.length)]);
		try {
			connection.play(filePath);
		} catch (e) {
			console.error(e);
		}
	});
}


function randInt(min:number, max:number) {
	return Math.floor(Math.random() * (max - min + 1)) + min;
}
